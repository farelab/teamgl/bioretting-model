%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~%
%          This function calculates the model equations 
% based on parameter values (x), initial value of variables, records of 
% temperature and plant material water content 
%       The microbial growth is simulated with a Monod equation
%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~%

function [dateday, C1sim, C2sim, C3sim, C4sim, C5sim, C6sim, C7sim, C8sim, X1sim, X2sim, X3sim, X4sim, X5sim, X6sim, X7sim, X8sim, Xtotsim,CO2sim, LCIsim, RLsim, FTempsim, FMoistsim]=CalculMonod(x,initialvalue,temp,moist)
global timesim step T

% preallocation of the vector used for equations (in step)
C1eq=zeros(timesim*5/step,1);   % pool C1 (Parenchyma)
C2eq=zeros(timesim*5/step,1);   % pool C2 (Parenchyma)
C3eq=zeros(timesim*5/step,1);   % pool C3 (Parenchyma)
C4eq=zeros(timesim*5/step,1);   % pool C4 (Parenchyma)
C5eq=zeros(timesim*5/step,1);   % pool C5 (Fiber bundle)
C6eq=zeros(timesim*5/step,1);   % pool C6 (Fiber bundle)
C7eq=zeros(timesim*5/step,1);   % pool C7 (Fiber bundle)
C8eq=zeros(timesim*5/step,1);   % pool C8 (Fiber bundle)
X1eq=zeros(timesim*5/step,1);   % microbial biomass X1 growing on C1
X2eq=zeros(timesim*5/step,1);   % microbial biomass X2 growing on C2
X3eq=zeros(timesim*5/step,1);   % microbial biomass X3 growing on C3
X4eq=zeros(timesim*5/step,1);   % microbial biomass X4 growing on C4
X5eq=zeros(timesim*5/step,1);   % microbial biomass X5 growing on C5
X6eq=zeros(timesim*5/step,1);   % microbial biomass X6 growing on C6
X7eq=zeros(timesim*5/step,1);   % microbial biomass X7 growing on C7
X8eq=zeros(timesim*5/step,1);   % microbial biomass X8 growing on C8
Xtoteq=zeros(timesim*5/step,1); % total microbial biomass (X1-8)
CO2eq=zeros(timesim*5/step,1);  % mineralized CO2 by total microbial biomass
LCIeq=zeros(timesim*5/step,1);  % lignocellulose index (lignin amoung
                                % cell wall compounds)
                                % LCI = C5/(C3+C4+C5+C6+C7+C8)
UpX1C1eq=zeros(timesim*5/step,1); % C uptake from C1 by biomass X1
UpX2C2eq=zeros(timesim*5/step,1); % C uptake from C2 by biomass X2 
UpX3C3eq=zeros(timesim*5/step,1); % C uptake from C3 by biomass X3
UpX4C4eq=zeros(timesim*5/step,1); % C uptake from C4 by biomass X4
UpX5C5eq=zeros(timesim*5/step,1); % C uptake from C5 by biomass X5
UpX6C6eq=zeros(timesim*5/step,1); % C uptake from C6 by biomass X6
UpX7C7eq=zeros(timesim*5/step,1); % C uptake from C7 by biomass X7
UpX8C8eq=zeros(timesim*5/step,1); % C uptake from C8 by biomass X8
CO2fluxeq=zeros(timesim*5/step,1);% CO2 flux

%preallocation of the variables used to record simulations (in day)
datesim=zeros(timesim+1,1);      % simulated dates in hour
dateday=zeros(timesim+1,1);      % simulated dates in day
C1sim=zeros(timesim+1,1);        % simulation of pool C1
C2sim=zeros(timesim+1,1);        % simulation of pool C2
C3sim=zeros(timesim+1,1);        % simulation of pool C3
C4sim=zeros(timesim+1,1);        % simulation of pool C4
C5sim=zeros(timesim+1,1);        % simulation of pool C5
C6sim=zeros(timesim+1,1);        % simulation of pool C6
C7sim=zeros(timesim+1,1);        % simulation of pool C7
C8sim=zeros(timesim+1,1);        % simulation of pool C8
X1sim=zeros(timesim+1,1);        % simulation of pool X1
X2sim=zeros(timesim+1,1);        % simulation of pool X2
X3sim=zeros(timesim+1,1);        % simulation of pool X3
X4sim=zeros(timesim+1,1);        % simulation of pool X4
X5sim=zeros(timesim+1,1);        % simulation of pool X5
X6sim=zeros(timesim+1,1);        % simulation of pool X6
X7sim=zeros(timesim+1,1);        % simulation of pool X7
X8sim=zeros(timesim+1,1);        % simulation of pool X8
Xtotsim=zeros(timesim+1,1);      % simulation of pool total microbial biomass
CO2sim=zeros(timesim+1,1);       % simulation of CO2
LCIsim=zeros(timesim+1,1);       % simulation of LCI
RLsim=zeros(timesim+1,1);        % simulation of retardation of degradation
                               % because of lignin protection
FTempsim=zeros(timesim+1,1);     % temperature function fT
FMoistsim=zeros(timesim+1,1);    % water content function fW

% Attribution of the parameter values
umaxd=x(1);   % Maximum specific microbial growth rate (day-1)
K1=x(2);      % Saturation constant for microbial growth on C1
K2=x(3);      % Saturation constant for microbial growth on C2
K3=x(4);      % Saturation constant for microbial growth on C3
K4=x(5);      % Saturation constant for microbial growth on C4
K5=x(6);      % Saturation constant for microbial growth on C5
K6=x(7);      % Saturation constant for microbial growth on C6
K7=x(8);      % Saturation constant for microbial growth on C7
K8=x(9);      % Saturation constant for microbial growth on C8
Y=x(10);      % Assimilation yield of microbial biomass
LCIT=x(14);   % Lignocellulose index threshold of the lignin control 
              % on microbial growth on fiber bundle
rL=x(15);     % Retardation factor of the lignin control on microbial growth 
              % in fiber bundle 
Tref=x(16);   % Reference temperature of the temperature function fT
tetaS=x(17);  % Water content of plant material at saturation of the tissue
tetaTh=x(18); % Water content of plant material at which microbial activity 
              % ceases
alpha=x(19);  % Shape parameter of the moisture function fW

% Conversion of the parameter in day to step
umax=umaxd/120;    % Maximum specific microbial growth rate in step

% Initialization of the variables used for equations with experimental data
C1eq(1)=initialvalue(1);       % simulation of pool C1
C2eq(1)=initialvalue(2);       % simulation of pool C2
C3eq(1)=initialvalue(3);       % simulation of pool C3
C4eq(1)=initialvalue(4);       % simulation of pool C4
C5eq(1)=initialvalue(5);       % simulation of pool C5
C6eq(1)=initialvalue(6);       % simulation of pool C6
C7eq(1)=initialvalue(7);       % simulation of pool C7
C8eq(1)=initialvalue(8);       % simulation of pool C8
X1eq(1)=initialvalue(9);       % simulation of pool X1
X2eq(1)=initialvalue(10);      % simulation of pool X2
X3eq(1)=initialvalue(11);      % simulation of pool X3
X4eq(1)=initialvalue(12);      % simulation of pool X4
X5eq(1)=initialvalue(13);      % simulation of pool X5
X6eq(1)=initialvalue(14);      % simulation of pool X6
X7eq(1)=initialvalue(15);      % simulation of pool X7
X8eq(1)=initialvalue(16);      % simulation of pool X8
Xtoteq(1)=initialvalue(17);    % simulation of pool total microbial biomass
CO2eq(1)=initialvalue(18);     % simulation of CO2
UpX1C1eq(1)=0;                 % C uptake from C1 by biomass X1
UpX2C2eq(1)=0;                 % C uptake from C2 by biomass X2
UpX3C3eq(1)=0;                 % C uptake from C3 by biomass X3
UpX4C4eq(1)=0;                 % C uptake from C4 by biomass X4
UpX5C5eq(1)=0;                 % C uptake from C5 by biomass X5
UpX6C6eq(1)=0;                 % C uptake from C6 by biomass X6
UpX7C7eq(1)=0;                 % C uptake from C7 by biomass X7
UpX8C8eq(1)=0;                 % C uptake from C8 by biomass X8
CO2fluxeq(1)=0;                % CO2 flux


% Initialization of the variables used for simulation with experimental
% data
C1sim(1)=initialvalue(1);       % simulation of pool C1
C2sim(1)=initialvalue(2);       % simulation of pool C2
C3sim(1)=initialvalue(3);       % simulation of pool C3
C4sim(1)=initialvalue(4);       % simulation of pool C4
C5sim(1)=initialvalue(5);       % simulation of pool C5
C6sim(1)=initialvalue(6);       % simulation of pool C6
C7sim(1)=initialvalue(7);       % simulation of pool C7
C8sim(1)=initialvalue(8);       % simulation of pool C8
X1sim(1)=initialvalue(9);       % simulation of pool X1
X2sim(1)=initialvalue(10);      % simulation of pool X2
X3sim(1)=initialvalue(11);      % simulation of pool X3
X4sim(1)=initialvalue(12);      % simulation of pool X4
X5sim(1)=initialvalue(13);      % simulation of pool X5
X6sim(1)=initialvalue(14);      % simulation of pool X6
X7sim(1)=initialvalue(15);      % simulation of pool X7
X8sim(1)=initialvalue(16);      % simulation of pool X8
Xtotsim(1)=initialvalue(17);    % simulation of pool total microbial biomass
CO2sim(1)=initialvalue(18);     % simulation of CO2


% Initialization of lignocellulose index
LCIsim(1)=initialvalue(5)/(initialvalue(3)+initialvalue(4)+initialvalue(5)+initialvalue(6)+initialvalue(7)+initialvalue(8));

    if LCIsim(1)<LCIT % if the initial lignocellulose index is less than 
                      % LCIT
    RLsim(1)=rL;      % the retardation factor is taken into account in the 
                      % equations
    else              % else
    RLsim(1)=1;       % the retardation factor is not taken into account
    end
    
% Initialization of the simulation date
datesim(1)=0;
dateday(1)=0;

% Initialization of the temperature function fT
FTempsim(1)=25/(1+(25-1)*exp(0.12*(Tref-temp(1))));

% Initialization of the moisture function fW
FMoistsim(1)=max(1-(log(tetaS/moist(1))/log(tetaS/tetaTh))^alpha,0);

  

% Calculation of the equation by discretization

for h=1:(timesim-1)             % h is the increment in hour
datesim(h+1)=h;                 % the vector with date is filled
dateday(h+1)=datesim(h+1)/24;

% Calulation of the temperature function fT
% fT = a/(1+(a-1)*exp(b*(Tref-T)))  with a=25, and b=0.12
% Tref is reference temperature entered in parameter values
% T is daily record temperature (default value is Tref)
FTempsim(h+1)=25/(1+(25-1)*exp((0.12)*(Tref-temp(h+1))));

% Calculation of the moisture function fW
% fW = 1-[ln(tetaS/teta)/ln(tetaS/tetaTh)]^aplha
% with tetaS the water content of plant material at saturation of the
% tissue, tetaTH the water content of plant material at which microbial 
% activity ceases, alpha the shape parameter of the moisture function fW, 
% teta the daily record of water content of plant material (default value
% is tetaS
FMoistsim(h+1)=max(1-(log(tetaS/moist(h+1))/log(tetaS/tetaTh))^alpha,0);


  for k=1:(5/step)         % k is the increment in step
    
   % Calculation of the lignocellulose index vector 
   % LIH = C5/(C3+C4+C5+C6+C7+C8)
   LCIeq(k+(h-1)/T)=(C5eq(k+(h-1)/T))/(C3eq(k+(h-1)/T)+C4eq(k+(h-1)/T)+C5eq(k+(h-1)/T)+C6eq(k+(h-1)/T)+C7eq(k+(h-1)/T)+C8eq(k+(h-1)/T));
    
       if LCIeq(k+(h-1)/T)<LCIT  % If the lignocellulose index is less 
                                 % than Lignocellulose index threshold
       kreq=rL;                  % The retardation factor of lignin control
                                 % on decomposition (rL) is taken into
                                 % account 
       else 
       kreq=1;                   % esle rL = 1 (no effect)
       end
    
       
    % Calulation of the C a uptake from substrate Ci in Parenchyma 
    % by biomass Xi UpXiCi(i=1,2,3,4)
    % UpXiCi = �max * Xi*fT* fW* Ci/(Ki+Ci)
    UpX1C1eq(k+1+(h-1)/T)=umax*X1eq(k+(h-1)/T)*FTempsim(h+1)*FMoistsim(h+1)*(C1eq(k+(h-1)/T)/(K1+C1eq(k+(h-1)/T)));
    UpX2C2eq(k+1+(h-1)/T)=umax*X2eq(k+(h-1)/T)*FTempsim(h+1)*FMoistsim(h+1)*(C2eq(k+(h-1)/T)/(K2+C2eq(k+(h-1)/T))); 
    UpX3C3eq(k+1+(h-1)/T)=umax*X3eq(k+(h-1)/T)*FTempsim(h+1)*FMoistsim(h+1)*(C3eq(k+(h-1)/T)/(K3+C3eq(k+(h-1)/T))); 
    UpX4C4eq(k+1+(h-1)/T)=umax*X4eq(k+(h-1)/T)*FTempsim(h+1)*FMoistsim(h+1)*(C4eq(k+(h-1)/T)/(K4+C4eq(k+(h-1)/T))); 
    
    % Calulation of the C a uptake from substrate Ci in fiber bundle
    % by biomass Xi UpXiCi(i=5,6,7,8)
    % UpXiCi = �max * kr* Xi*fT* fW* Ci/(Ki+Ci)
    UpX5C5eq(k+1+(h-1)/T)=umax*kreq*X5eq(k+(h-1)/T)*FTempsim(h+1)*FMoistsim(h+1)*(C5eq(k+(h-1)/T)/(K5+C5eq(k+(h-1)/T)));
    UpX6C6eq(k+1+(h-1)/T)=umax*kreq*X6eq(k+(h-1)/T)*FTempsim(h+1)*FMoistsim(h+1)*(C6eq(k+(h-1)/T)/(K6+C6eq(k+(h-1)/T)));
    UpX7C7eq(k+1+(h-1)/T)=umax*kreq*X7eq(k+(h-1)/T)*FTempsim(h+1)*FMoistsim(h+1)*(C7eq(k+(h-1)/T)/(K7+C7eq(k+(h-1)/T)));
    UpX8C8eq(k+1+(h-1)/T)=umax*kreq*X8eq(k+(h-1)/T)*FTempsim(h+1)*FMoistsim(h+1)*(C8eq(k+(h-1)/T)/(K8+C8eq(k+(h-1)/T)));
    
    % Calculation of the CO2 flux
    % CO2flux = (1-Y)/Y * sum UpXiCi
    CO2fluxeq(k+1+(h-1)/T)=((1-Y)/Y)*(UpX1C1eq(k+1+(h-1)/T)+UpX2C2eq(k+1+(h-1)/T)+UpX3C3eq(k+1+(h-1)/T)+UpX4C4eq(k+1+(h-1)/T)+UpX5C5eq(k+1+(h-1)/T)+UpX6C6eq(k+1+(h-1)/T)+UpX7C7eq(k+1+(h-1)/T)+UpX8C8eq(k+1+(h-1)/T));
    
    % Calculation of the total biomass pools
    % Xtot = sum Xi (i=1-8)
    Xtoteq(k+(h-1)/T)=X1eq(k+(h-1)/T)+X2eq(k+(h-1)/T)+X3eq(k+(h-1)/T)+X4eq(k+(h-1)/T)+X5eq(k+(h-1)/T)+X6eq(k+(h-1)/T)+X7eq(k+(h-1)/T)+X8eq(k+(h-1)/T);
    
    % Calculation of the dynamic of biomass pools
    % Xi(t+1)= Xi(t)+UpXiCi (i=1-8)
    X1eq(k+1+(h-1)/T)=X1eq(k+(h-1)/T)+step*UpX1C1eq(k+1+(h-1)/T);
    X2eq(k+1+(h-1)/T)=X2eq(k+(h-1)/T)+step*UpX2C2eq(k+1+(h-1)/T);
    X3eq(k+1+(h-1)/T)=X3eq(k+(h-1)/T)+step*UpX3C3eq(k+1+(h-1)/T);
    X4eq(k+1+(h-1)/T)=X4eq(k+(h-1)/T)+step*UpX4C4eq(k+1+(h-1)/T);
    X5eq(k+1+(h-1)/T)=X5eq(k+(h-1)/T)+step*UpX5C5eq(k+1+(h-1)/T);
    X6eq(k+1+(h-1)/T)=X6eq(k+(h-1)/T)+step*UpX6C6eq(k+1+(h-1)/T);
    X7eq(k+1+(h-1)/T)=X7eq(k+(h-1)/T)+step*UpX7C7eq(k+1+(h-1)/T);
    X8eq(k+1+(h-1)/T)=X8eq(k+(h-1)/T)+step*UpX8C8eq(k+1+(h-1)/T);
   
   
    % Calculation of the dynamic of substrate pools
    % Ci(t+1)= Ci(t)-(1/Y)*UpXiCi (i=1-8)
    C1eq(k+1+(h-1)/T)=C1eq(k+(h-1)/T)-step*UpX1C1eq(k+1+(h-1)/T)/Y;
    C2eq(k+1+(h-1)/T)=C2eq(k+(h-1)/T)-step*UpX2C2eq(k+1+(h-1)/T)/Y;
    C3eq(k+1+(h-1)/T)=C3eq(k+(h-1)/T)-step*UpX3C3eq(k+1+(h-1)/T)/Y;
    C4eq(k+1+(h-1)/T)=C4eq(k+(h-1)/T)-step*UpX4C4eq(k+1+(h-1)/T)/Y;
    C5eq(k+1+(h-1)/T)=C5eq(k+(h-1)/T)-step*UpX5C5eq(k+1+(h-1)/T)/Y;
    C6eq(k+1+(h-1)/T)=C6eq(k+(h-1)/T)-step*UpX6C6eq(k+1+(h-1)/T)/Y;
    C7eq(k+1+(h-1)/T)=C7eq(k+(h-1)/T)-step*UpX7C7eq(k+1+(h-1)/T)/Y;
    C8eq(k+1+(h-1)/T)=C8eq(k+(h-1)/T)-step*UpX8C8eq(k+1+(h-1)/T)/Y;
    
    % Calculation of CO2 respiration
    % CO2(t+1)=CO2(t)+ CO2flux
    CO2eq(k+1+(h-1)/T)=CO2eq(k+(h-1)/T)+step*CO2fluxeq(k+1+(h-1)/T);
  
   end

% Conversion of the simulated variables in step in the variable in day
C1sim(h+1)=C1eq(5/step+(h-1)/T);
C2sim(h+1)=C2eq(5/step+(h-1)/T);
C3sim(h+1)=C3eq(5/step+(h-1)/T);
C4sim(h+1)=C4eq(5/step+(h-1)/T);
C5sim(h+1)=C5eq(5/step+(h-1)/T);
C6sim(h+1)=C6eq(5/step+(h-1)/T);
C7sim(h+1)=C7eq(5/step+(h-1)/T);       
C8sim(h+1)=C8eq(5/step+(h-1)/T);       
X1sim(h+1)=X1eq(5/step+(h-1)/T);       
X2sim(h+1)=X2eq(5/step+(h-1)/T);       
X3sim(h+1)=X3eq(5/step+(h-1)/T);     
X4sim(h+1)=X4eq(5/step+(h-1)/T);   
X5sim(h+1)=X5eq(5/step+(h-1)/T);        
X6sim(h+1)=X6eq(5/step+(h-1)/T);        
X7sim(h+1)=X7eq(5/step+(h-1)/T);   
X8sim(h+1)=X8eq(5/step+(h-1)/T);      
Xtotsim(h+1)=Xtoteq(5/step+(h-1)/T);   
CO2sim(h+1)=CO2eq(5/step+(h-1)/T);    
LCIsim(h+1)=LCIeq(5/step+(h-1)/T);
RLsim(h+1)=kreq;


end

    
end