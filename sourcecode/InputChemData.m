%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~%
%  This function splits up into independent vectors the matrice with %
%   experimental data from the Excel file related to chemistry,      %
%   mass loss, and Parenchyma to Fiber bundle ratio.
%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~%

function [dateexp, SOLexp, CellWallexp, SUGexp, GLUexp, LIGexp, Otherexp, MassLossexp,PtoFratioexp]=InputChemData(x)

global datenbChem
% datenbChem is the number of experimental sampling dates

dateexp=x(1:datenbChem,1);     % experimental samlpling dates
SOLexp=x(1:datenbChem,2);      % experimental soluble content (% DM)
CellWallexp=x(1:datenbChem,3); % experimental cell wall content (% DM)
SUGexp=x(1:datenbChem,4);      % experimental sugar content (% cell wall)
GLUexp=x(1:datenbChem,5);      % experimental glucose content (% cell wall)
LIGexp=x(1:datenbChem,6);      % experimental lignin content (% cell wall)
Otherexp=x(1:datenbChem,7);    % experimental other compounds (% cell wall)
MassLossexp=x(1:datenbChem,8); % experimenta mass loss (% initial DM). 
                               % The mass loss is considered to be CO2 loss
PtoFratioexp=x(1:datenbChem,9);% experimental proportion of parenchyma cell 
                               % wall in outer tissue

%tableexpChem(1:datenbChem,1:9)=[dateexp, SOLexp, CellWallexp, SUGexp, GLUexp, LIGexp, Otherexp, MassLossexp, PtoFratioexp];

end