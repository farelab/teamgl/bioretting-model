%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~%
% This function splits up the matrice (with information on parameters)
% into vectors :
% - the user's choice for optimization or simulation
% - initial value (if optimized) or value for parameters (x0)
% - the minimum value for parameter allowed during optimization (lb)
% - the maximum value for parameter allowed during optimization (ub)
%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~%

function [x0,lb,ub]=InputParam(x)
global paramnb

%preallocation of the vectors used to record information on parameters
x0=zeros(paramnb,1); % Initial value (if optimized) or value for parameters 
lb=zeros(paramnb,1); % Minimum value for parameters allowed during 
                     % optimization
ub=zeros(paramnb,1); % Maximum value for parameters allowed during 
                     % optimization


for i=1:paramnb
    x0(i)=x(i,2);     % Vector x0 is filled with the initial value 
                      % or value of the parameter i
    
    if x(i,1)==1      % If the user chooses to optimize parameter i
        lb(i)=x(i,3); % Vector lb is filled with the minium value allowed 
                      % for parameter i
        ub(i)=x(i,4); % Vector ub is filled with the maximum value allowed 
                      % for parameter i
    else              % esle (if the user chooses not to optimize parameter i)
        lb(i)=x(i,2); % Vector lb is filled with the value of parameter i
        ub(i)=x(i,2); % Vector ub is filled with the value of parameter i
    end
end

end