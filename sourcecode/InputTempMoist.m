%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~%
% This function splits up the matrice x with information on temperature and 
% plant water content records into vectors :
% - temp : the daily experimental record of temperature
% - moist : the daily experimental record of plant water content
% if the matrice is filled with 0, reference temperature is taken as
% default temperature value in the vector y, and water content at plant tissue 
% saturation is taken as default moisture value
%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~%

function [temp,moist]=InputTempMoist(x,y)
global timesim

% Preallocation of the vectors used to record information on temperature and
% moisture experimental records
temp=zeros(timesim,1);   % Daily experimental record of temperature
moist=zeros(timesim,1);  % Daily experimental record of plant water content


for i=1:timesim  % for i to the end of simulation duration (in day)
   if x(i,1)~=0          % if temperature record is no equal to 0
    temp(i)=x(i,1);      % then temp vector is filled with the temperature 
                         % record of day i
   else
    temp (i)=y(16);      % else temp vector is filled the reference 
                         % temperature entered in the parameter values
   end
   
   if x(i,2)~=0          % if moisture record is no equal to 0
    moist(i)=x(i,2);     % then moist vector is filled with the water content
                         % record of day i
   else
    moist (i)=y(17);     % else moist vector is filled the plant tissue 
                         % water content at saturation entered in the parameter
                         % values
   end
   
end

   