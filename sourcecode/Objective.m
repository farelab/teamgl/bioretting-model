%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~%
% This function calculates the total root mean square error between
% simulations and experimental data (the objective function of the
% optimization procedure). 
% - x is the vector with parameter values
% - tableexpChem is the vector with experimental data related to chemistry,
% masse loss and parenchyma to fiber bundle ratio
% - initialvalue is the vector with initial values for variables
% - weight is the vector with the weight of variables that the user want for
% the different variables in the objective function.
%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~%

function [RRMSE_tot]=Objective(x,dataChem,initialvalue,temp, moist, weight)

global datenbChem execution 

if execution ==3
% Call of the function CalculMonod that calculates equations simulating
% microbial growth with Monod kinetic
[~, C1sim, C2sim, C3sim, C4sim, C5sim, C6sim, C7sim, C8sim, ~, ~, ~, ~, ~, ~, ~, ~, Xtotsim,CO2sim, ~, ~, ~, ~]=CalculMonod(x,initialvalue,temp, moist);
end

if execution ==4
% Call of the function CalculTavares that calculates equations simulating
% microbial growth with Monod-Two-Phase equation

[~, C1sim, C2sim, C3sim, C4sim, C5sim, C6sim, C7sim, C8sim, ~, ~, ~, ~, ~, ~, ~, ~, Xtotsim,CO2sim, ~, ~, ~, ~, ~]=CalculMonodTwoPhase(x,initialvalue,temp,moist);   
 
end

[dateexp, SOLexp, CellWallexp, SUGexp, GLUexp, LIGexp, Otherexp, MassLossexp, PtoFratioexp]=InputChemData(dataChem);

% Preallocation of the variable used to record observed data, calculated 
% based on biochemical and loss weight data, and simulated biomass
C1C2obs=zeros(datenbChem,1);
C3obs=zeros(datenbChem,1);
C4obs=zeros(datenbChem,1);
C5obs=zeros(datenbChem,1);
C6obs=zeros(datenbChem,1);
C7obs=zeros(datenbChem,1);
C8obs=zeros(datenbChem,1);
CO2obs=zeros(datenbChem,1);

% Initialization of the variable used to record observed data
    % C1+C2 obs = Solubles * (100 - Xtot - MassLoss)/100
C1C2obs(1)=SOLexp(1)*(100-Xtotsim(1)-MassLossexp(1))/100;

    % C3 obs = Sugars * (Cell Wall content /100)* (parenchyma-cell wall proportion /100)*(100 - Xtot - MassLoss)/100
C3obs(1)=SUGexp(1)*(CellWallexp(1)/100)*(PtoFratioexp(1)/100)*((100-Xtotsim(1)-MassLossexp(1))/100);

    % C4 obs = Other compounds * (Cell Wall content /100)* (parenchyma-cell wall proportion /100)*(100 - Xtot - MassLoss)/100
C4obs(1)=Otherexp(1)*(CellWallexp(1)/100)*(PtoFratioexp(1)/100)*((100-Xtotsim(1)-MassLossexp(1))/100);

    % C5 obs = Lignin * (Cell Wall content /100)* (100 - Xtot - MassLoss)/100
C5obs(1)=LIGexp(1)*(CellWallexp(1)/100)*((100-Xtotsim(1)-MassLossexp(1))/100);

    % C6 obs = (Sugars - Glucose) * (Cell Wall content /100)* (fiber bundle-cell wall proportion/100) *(100 - Xtot - MassLoss)/100
C6obs(1)=(SUGexp(1)-GLUexp(1))*(CellWallexp(1)/100)*((100-PtoFratioexp(1))/100)*((100-Xtotsim(1)-MassLossexp(1))/100);

    % C7 obs = Other compounds * (Cell Wall content /100)* (fiber bundle-cell wall proportion/100) *(100 - Xtot - MassLoss)/100
C7obs(1)=Otherexp(1)*(CellWallexp(1)/100)*((100-PtoFratioexp(1))/100)*((100-Xtotsim(1)-MassLossexp(1))/100);

    % C8 obs = Glucose * (Cell Wall content /100)* (fiber bundle-cell wall proportion/100) *(100 - Xtot - MassLoss)/100
C8obs(1)=GLUexp(1)*(CellWallexp(1)/100)*((100-PtoFratioexp(1))/100)*((100-Xtotsim(1)-MassLossexp(1))/100);

    % CO2 obs = Mass Loss
CO2obs(1)=MassLossexp(1);

% Preallocation of the variables used to simulate the square errors
% calculated between observed and simulated kinetics at each sampling dates
C1C2se=zeros(datenbChem,1);    
C3se=zeros(datenbChem,1);       
C4se=zeros(datenbChem,1);           
C5se=zeros(datenbChem,1);      
C6se=zeros(datenbChem,1);      
C7se=zeros(datenbChem,1);       
C8se=zeros(datenbChem,1);      
CO2se=zeros(datenbChem,1);  

% Preallocation of the variables used to calculate the sum of the square 
% error attributed to variables
sum_C1C2se=zeros(datenbChem,1);    
sum_C3se=zeros(datenbChem,1);       
sum_C4se=zeros(datenbChem,1);       
sum_C5se=zeros(datenbChem,1);     
sum_C6se=zeros(datenbChem,1);             
sum_C7se=zeros(datenbChem,1);       
sum_C8se=zeros(datenbChem,1);      
sum_CO2se=zeros(datenbChem,1); 

% Initialization of the variables used to simulate the square errors
% calculated between observed and simulated kinetics at each sampling dates
C1C2se(1)=(C1sim(1)+C2sim(1)-C1C2obs(1))^2;
C3se(1)=(C3sim(1)-C3obs(1))^2;
C4se(1)=(C4sim(1)-C4obs(1))^2;
C5se(1)=(C5sim(1)-C5obs(1))^2;
C6se(1)=(C6sim(1)-C6obs(1))^2;
C7se(1)=(C7sim(1)-C7obs(1))^2;
C8se(1)=(C8sim(1)-C8obs(1))^2;
CO2se(1)=0;

% Initialisation of the variables used to calculate the sum of the square 
% error attributed to variables
sum_C1C2se(1)=C1C2se(1);
sum_C3se(1)=C3se(1);
sum_C4se(1)=C4se(1);
sum_C5se(1)=C5se(1);
sum_C6se(1)=C6se(1);
sum_C7se(1)=C7se(1);
sum_C8se(1)=C8se(1);
sum_CO2se(1)=CO2se(1);

% Caculation of the variables used to record observed data, square errors
% calculated between observed and simulated kinetics, sum of the square
% error attributed to each variable 
for s=2:datenbChem
    
    % Calulation of the Parenchyma-cell wall proportion in relation to the
    % outer tissue-cell wall
    % PtFratio = (C3sim + C4 sim)*100 / (C5sim + C6sim + C7sim + C8sim)
    PtoFratioexp(s)=(C3sim((dateexp(s)*24)+1)+C4sim((dateexp(s)*24)+1))*100/(C5sim((dateexp(s)*24)+1)+C6sim((dateexp(s)*24)+1)+C7sim((dateexp(s)*24)+1)+C8sim((dateexp(s)*24)+1));
    
    % C1+C2 obs = Solubles * (100 - Xtot - MassLoss)/100
    C1C2obs(s)=SOLexp(s)*(100-Xtotsim((dateexp(s)*24)+1)-MassLossexp(s))/100;
    
    % C3 obs = Sugars * (Cell Wall content /100)* (parenchyma-cell wall proportion /100)*(100 - Xtot - MassLoss)/100
    C3obs(s)=SUGexp(s)*(CellWallexp(s)/100)*(PtoFratioexp(s)/100)*((100-Xtotsim((dateexp(s)*24)+1)-MassLossexp(s))/100);
    
    % C4 obs = Other compounds * (Cell Wall content /100)* (parenchyma-cell wall proportion /100)*(100 - Xtot - MassLoss)/100
    C4obs(s)=Otherexp(s)*(CellWallexp(s)/100)*(PtoFratioexp(s)/100)*((100-Xtotsim((dateexp(s)*24)+1)-MassLossexp(s))/100);
    
    % C5 obs = Lignin * (Cell Wall content /100)* (100 - Xtot - MassLoss)/100
    C5obs(s)=LIGexp(s)*(CellWallexp(s)/100)*((100-Xtotsim((dateexp(s)*24)+1)-MassLossexp(s))/100);
    
    % C6 obs = (Sugars - Glucose) * (Cell Wall content /100)* (fiber bundle-cell wall proportion/100) *(100 - Xtot - MassLoss)/100
    C6obs(s)=(SUGexp(s)-GLUexp(s))*(CellWallexp(s)/100)*((100-PtoFratioexp(s))/100)*((100-Xtotsim((dateexp(s)*24)+1)-MassLossexp(s))/100);
    
    % C7 obs = Other compounds * (Cell Wall content /100)* (fiber bundle-cell wall proportion/100) *(100 - Xtot - MassLoss)/100
    C7obs(s)=Otherexp(s)*(CellWallexp(s)/100)*((100-PtoFratioexp(s))/100)*((100-Xtotsim((dateexp(s)*24)+1)-MassLossexp(s))/100);
    
    % C8 obs = Glucose * (Cell Wall content /100)* (fiber bundle-cell wall proportion/100) *(100 - Xtot - MassLoss)/100
    C8obs(s)=GLUexp(s)*(CellWallexp(s)/100)*((100-PtoFratioexp(s))/100)*((100-Xtotsim((dateexp(s)*24)+1)-MassLossexp(s))/100);
    
    % CO2 obs = Mass Loss
    CO2obs(s)=MassLossexp(s);

    % Calculation of the square errors between observed and simulated
    % variables
    C1C2se(s)=(C1sim((dateexp(s))*24+1)+C2sim((dateexp(s))*24+1)-C1C2obs(s))^2;
    C3se(s)=(C3sim((dateexp(s)*24)+1)-C3obs(s))^2;
    C4se(s)=(C4sim((dateexp(s))*24+1)-C4obs(s))^2;
    C5se(s)=(C5sim((dateexp(s)*24)+1)-C5obs(s))^2;
    C6se(s)=(C6sim((dateexp(s)*24)+1)-C6obs(s))^2;
    C7se(s)=(C7sim((dateexp(s)*24)+1)-C7obs(s))^2;
    C8se(s)=(C8sim((dateexp(s)*24)+1)-C8obs(s))^2;
    CO2se(s)=(CO2sim((dateexp(s)*24)+1)-CO2obs(s))^2;
    
    % Calculation of the sum of square errors between observed and simulated
    % variables
    sum_C1C2se(s)=sum_C1C2se(s-1)+C1C2se(s);
    sum_C3se(s)=sum_C3se(s-1)+C3se(s);    
    sum_C4se(s)=sum_C4se(s-1)+C4se(s);   
    sum_C5se(s)=sum_C5se(s-1)+C5se(s);
    sum_C6se(s)=sum_C6se(s-1)+C6se(s);
    sum_C7se(s)=sum_C7se(s-1)+C7se(s);
    sum_C8se(s)=sum_C8se(s-1)+C8se(s);
    sum_CO2se(s)=sum_CO2se(s-1)+CO2se(s);

end

% Caculation of the relative root mean square error for each variable
% RRMSE C1C2 = square ((1/n)*sum (C1C1se)/((max (C1C2obs) - min
% (C1C2obs)))^2
RRMSE_C1C2=sqrt((1/datenbChem)*(sum_C1C2se(datenbChem)/(max(C1C2obs)-min(C1C2obs))^2));
RRMSE_C3=sqrt((1/datenbChem)*(sum_C3se(datenbChem)/(max(C3obs)-min(C3obs))^2));
RRMSE_C4=sqrt((1/datenbChem)*(sum_C4se(datenbChem)/(max(C4obs)-min(C4obs))^2));
RRMSE_C5=sqrt((1/datenbChem)*(sum_C5se(datenbChem)/(max(C5obs)-min(C5obs))^2));
RRMSE_C6=sqrt((1/datenbChem)*(sum_C6se(datenbChem)/(max(C6obs)-min(C6obs))^2));
RRMSE_C7=sqrt((1/datenbChem)*(sum_C7se(datenbChem)/(max(C7obs)-min(C7obs))^2));
RRMSE_C8=sqrt((1/datenbChem)*(sum_C8se(datenbChem)/(max(C8obs)-min(C8obs))^2));
RRMSE_CO2=sqrt((1/datenbChem)*(sum_CO2se(datenbChem)/(max(CO2obs)-min(CO2obs))^2));


% Caculation of the sum of the total relative root mean sqaure error
RRMSE_tot=weight(1)*RRMSE_C1C2+weight(3)*RRMSE_C3+weight(4)*RRMSE_C4+weight(5)*RRMSE_C5+weight(6)*RRMSE_C6+weight(7)*RRMSE_C7+weight(8)*RRMSE_C8+weight(18)*RRMSE_CO2;

end