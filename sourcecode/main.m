% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~%
% This is the main program which excecutes all functions %
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~%

close all
clc

global step T timesim paramnb varnb datenbChem varnbOpt execution

% Importation of the user's choice for biological equation, simulation, 
% optimization, increment and duration
filename = 'RETTING.xlsx';
choice=xlsread(filename,'Options','E2:E4');
execution=choice(1);    % user's choice for simulation or optimisation with 
                        % degradation equation of Monod or Monod-two-phase
                        % (Tavares)
step=choice(2);         % model increment (in hour)
timeday=choice(3);      % last date of the retting (in day)
timesim=(timeday+1)*24; % simulation duration (in hour)
T=step/5;              % model increment (in step)


% Record the number of model parameters
paramnb=xlsread(filename,'Parameters_InitialVariables','D2');

% Record the number of model variables
varnb=xlsread(filename,'Parameters_InitialVariables','D26');

% record the number of experimental date
datenbChem=xlsread(filename,'ChemData','A4');

% record the number of variables used to optimization 
varnbOpt=xlsread(filename,'Parameters_InitialVariables','D27');


%preallocation of the varibales used to record simulations (in day)
datesim=zeros(timesim,1);      % simulated dates
C1sim=zeros(timesim,1);        % simulation of pool C1
C2sim=zeros(timesim,1);        % simulation of pool C2
C3sim=zeros(timesim,1);        % simulation of pool C3
C4sim=zeros(timesim,1);        % simulation of pool C4
C5sim=zeros(timesim,1);        % simulation of pool C5
C6sim=zeros(timesim,1);        % simulation of pool C6
C7sim=zeros(timesim,1);        % simulation of pool C7
C8sim=zeros(timesim,1);        % simulation of pool C8
X1sim=zeros(timesim,1);        % simulation of pool X1
X2sim=zeros(timesim,1);        % simulation of pool X2
X3sim=zeros(timesim,1);        % simulation of pool X3
X4sim=zeros(timesim,1);        % simulation of pool X4
X5sim=zeros(timesim,1);        % simulation of pool X5
X6sim=zeros(timesim,1);        % simulation of pool X6
X7sim=zeros(timesim,1);        % simulation of pool X7
X8sim=zeros(timesim,1);        % simulation of pool X8
Xtotsim=zeros(timesim,1);      % simulation of pool total microbial biomass
CO2sim=zeros(timesim,1);       % simulation of CO2
LCIsim=zeros(timesim,1);       % simulation of lignocellulose index
RLsim=zeros(timesim,1);        % simulation retardation factor
FTempsim=zeros(timesim,1);     % simulation of temperature function
Tavlimsim=zeros(timesim,1);    % simulation of Tavares limitation factor

% Importation of the information on parameters from Excel file/ sheet 
% "Parameters_VariablesInitiales" in the 'param' matrice
param=xlsread(filename,'Parameters_InitialVariables','E4:H22');

% Call of the Inputparam function which defines the initial value for
% parameters, their minimum and maximum values allowed during optimization
% x0 = Initial value (if optimized) or value for parameters
% lb = Minimum value for parameters allowed during optimization
% ub= Maximum value for parameters allowed during optimization
[x0,lb,ub]=InputParam(param);

% Importation of the information on variables from Excel file / sheet
% "Parameters_VariablesInitiales" in the 'varinitial'
varinitial=xlsread(filename,'Parameters_InitialVariables','F29:F46');
[initialvalue]=InputInitialVar(varinitial);

% Importation of the daily experimental records of temperature and water content 
% from Excel file / sheet "Parameters_VariablesInitiales" in the 'tempinput'
tempmoist=xlsread(filename,'TempMoistData','E5:F2000');
[temp,moist]=InputTempMoist(tempmoist,x0);

% Importation of the weight attributed to each model variables during the
% optimization procedure
varweight=xlsread(filename,'Parameters_InitialVariables','E29:E46');
[weight]=VariableWeight(varweight);

% Importation of the experimenta data from the Excel file related to 
% chemistry, mass loss, and Parenchyma to Fiber bundle ratio.
dataChem=xlsread(filename,'ChemData','B5:J9');


% Call of the InputdataChem function splits up into vectors 
% the matrice with experimental related to chemistry, mass loss, 
% and Parenchyma to Fiber bundle ratio.
% dateexp= experimental samlpling dates
% SOLexp=  experimental soluble content (% DM)
% CellWallexp=  experimental cell wall content (% DM)
% SUGexp= experimental sugar content (% cell wall)
% GLUexp= experimental glucose content (% cell wall)
% LIGexp= experimental lignin content (% cell wall)
% Otherexp= experimental other compounds (% cell wall)
% MassLossexp= experimenta mass loss (% initial DM)(considered to be CO2
% loss)
% PtoFratioexp= experimental proportion of parenchyma cell wall in outer tissue
[dateexp, SOLexp, CellWallexp, SUGexp, GLUexp, LIGexp, Otherexp, MassLossexp,PtoFratioexp]=InputChemData(dataChem);

if execution==1      % If the user chooses to simulate using Monod equation 
                     % to model microbial growth (Choice 1)
    x=x0;            % values for parameters are default values
    
    %Simulation with Monod equation
    [dateday, C1sim, C2sim, C3sim, C4sim, C5sim, C6sim, C7sim, C8sim, X1sim, X2sim, X3sim, X4sim, X5sim, X6sim, X7sim, X8sim, Xtotsim,CO2sim,LCIsim,RLsim, FTempsim, FMoistsim]=CalculMonod(x,initialvalue,temp, moist);

    % Exportation of the simulations to the Excel file
    tablesimul=[dateday, C1sim, C2sim, C3sim, C4sim, C5sim, C6sim, C7sim, C8sim, X1sim, X2sim, X3sim, X4sim, X5sim, X6sim, X7sim, X8sim, Xtotsim,CO2sim,LCIsim,RLsim, FTempsim, FMoistsim];
    xlswrite(filename,tablesimul,'Simulations','B3');
end

if execution==2      % If the user chooses to simulate using Monod-Two-Phase equation
                     % equation to model microbial growth (Choice 2)
    x=x0;            % values for parameters are default values
    
   %Simulation with Monod-Two-Phase equation
   [dateday, C1sim, C2sim, C3sim, C4sim, C5sim, C6sim, C7sim, C8sim, X1sim, X2sim, X3sim, X4sim, X5sim, X6sim, X7sim, X8sim, Xtotsim,CO2sim,LCIsim,RLsim, FTempsim, FMoistsim, Tavlimsim]=CalculMonodTwoPhase(x,initialvalue,temp, moist);
   
   % Exportation of the simulations to the Excel file
   tablesimul=[dateday, C1sim, C2sim, C3sim, C4sim, C5sim, C6sim, C7sim, C8sim, X1sim, X2sim, X3sim, X4sim, X5sim, X6sim, X7sim, X8sim, Xtotsim,CO2sim,LCIsim,RLsim, FTempsim, FMoistsim, Tavlimsim];
   xlswrite(filename,tablesimul,'Simulations','B3');
   
end


if execution==3     % If the user chooses to optimize parameters using 
                    % Monod equation to model microbial growth (Choice 3)
                    
   % Options of optimization function
   options=optimset('DiffMinChange',1e-3,'DiffMaxChange',1,'Display','iter','TolFun',1e-30,'TolX',1e-30,'TolCon',1e-30,'Algorithm','active-set','MaxFunEvals',1000);
   
   % Call of optimization function 
   [x,fval,exitflag,output,lambda,grad,hessian]=fmincon(@(x)Objective(x,dataChem,initialvalue,temp,moist, weight),x0,[],[],[],[],lb,ub,[],options);
 
   % Exportation of the optimized parameters to the Excel file
   xlswrite(filename,x,'Parameters_InitialVariables','I4');
   
   % Simulation with the optimized parameters
   [dateday, C1sim, C2sim, C3sim, C4sim, C5sim, C6sim, C7sim, C8sim, X1sim, X2sim, X3sim, X4sim, X5sim, X6sim, X7sim, X8sim, Xtotsim,CO2sim,LCIsim,RLsim, FTempsim, FMoistsim]=CalculMonod(x,initialvalue,temp, moist);
   
   % Exportation of the simulations to the Excel file
   tablesimul=[dateday,C1sim, C2sim, C3sim, C4sim, C5sim, C6sim, C7sim, C8sim, X1sim, X2sim, X3sim, X4sim, X5sim, X6sim, X7sim, X8sim, Xtotsim,CO2sim,LCIsim,RLsim, FTempsim, FMoistsim];
   xlswrite(filename,tablesimul,'Simulations','B3');

end 


if execution ==4      % If the user chooses to optimize parameters using 
                      % Monod-Two-Phase equation to model microbial growth (Choice 4)
                      
    % Options of optimization function
    options=optimset('DiffMinChange',1e-3,'DiffMaxChange',1,'Display','iter','TolFun',1e-30,'TolX',1e-30,'TolCon',1e-30,'Algorithm','active-set','MaxFunEvals',1000);
    
    % Call of optimization function 
    [x,fval,exitflag,output,lambda,grad,hessian]=fmincon(@(x)Objective(x,dataChem,initialvalue,temp,moist, weight),x0,[],[],[],[],lb,ub,[],options);

    % Exportation of the optimized parameters to the Excel file
    xlswrite(filename,x,'Parameters_InitialVariables','I4');
    
    % Simulation with the optimized parameters
    [dateday, C1sim, C2sim, C3sim, C4sim, C5sim, C6sim, C7sim, C8sim, X1sim, X2sim, X3sim, X4sim, X5sim, X6sim, X7sim, X8sim, Xtotsim,CO2sim,LCIsim,RLsim, FTempsim, FMoistsim, Tavlimsim]=CalculMonodTwoPhase(x,initialvalue,temp, moist);
    
    % Exportation of the simulations to the Excel file
    tablesimul=[dateday, C1sim, C2sim, C3sim, C4sim, C5sim, C6sim, C7sim, C8sim, X1sim, X2sim, X3sim, X4sim, X5sim, X6sim, X7sim, X8sim, Xtotsim,CO2sim,LCIsim,RLsim, FTempsim, FMoistsim, Tavlimsim];
    xlswrite(filename,tablesimul,'Simulations','B3');

end


